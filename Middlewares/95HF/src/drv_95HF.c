/***************************************************************************//**
	@file		drv_95HF.c
	@brief		CR95HF or ST25R95 driver.
********************************************************************************

	This driver is used to control the CR95HF or the ST25R95 NFC transceiver
	over SPI bus.

	@see		https://www.st.com/en/embedded-software/stsw-m24lr007.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		September-2020.
*******************************************************************************/

#include "drv_95HF.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/**< General buffer. */
uint8_t	u95HFBuffer [RFTRANS_95HF_MAX_BUFFER_SIZE+3];

/**< Default set last IC version. */
IC_VERSION IcVers = QJE;

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/***************************************************************************//**
	@brief		Send a negative pulse on IRQ_IN pin to wake-up the device.
	@return		None.
*******************************************************************************/

void drv95HF_SendIRQINPulse(void) {

	/* Send a pulse on IRQ_IN: */
	DRV95HF_IO_CTRL_SET_IRQ_IN();
	DRV95HF_DELAY_MS(1);
	DRV95HF_IO_CTRL_CLR_IRQ_IN();
	DRV95HF_DELAY_MS(1);
	DRV95HF_IO_CTRL_SET_IRQ_IN();

	/* Need to wait 10ms after the pulse before to send the first command: */
	DRV95HF_DELAY_MS(10);

}

/***************************************************************************//**
	@brief		Reset command.

	To reset the device using the SPI, the application sends the SPI Reset
	command (control byte 01) which starts the internal controller reset process
	and puts the device into Power-up state.

	@return		None.
*******************************************************************************/

void drv95HF_ResetSPI(void) {

	/* Deselect the device over SPI: */
	DRV95HF_IO_CTRL_SET_SPI_SS();
	DRV95HF_DELAY_MS(1);
	/* Select the device over SPI: */
	DRV95HF_IO_CTRL_CLR_SPI_SS();
	/* Send reset control byte:	*/
	drv95HF_SendReceiveByte(DRV95HF_CONTROL_RESET);
	/* Deselect the device over SPI: */
	DRV95HF_IO_CTRL_SET_SPI_SS();

}

/***************************************************************************//**
	@brief		Useful routine used to wake-up the device.
	@return		None.
*******************************************************************************/

void drv95HF_WakeUp(void) {

	/* SPI reset: */
	drv95HF_ResetSPI();

	/* Send a pulse on IRQ_IN to wake-up the device: */
	drv95HF_SendIRQINPulse();
	
}

/***************************************************************************//**
	@brief		This function sends a command over SPI bus.
	@param		pData Data pointer (CMD + LEN + DATA).
	@return		None.
*******************************************************************************/

void drv95HF_SendSPICommand(uc8 *pData) {

	uint16_t bufferlength = 0;
	  	
	/* Select the device over SPI: */
	DRV95HF_IO_CTRL_CLR_SPI_SS();

	/* Send a sending request:  */
	drv95HF_SendReceiveByte(DRV95HF_CONTROL_SEND);

	/* Echo: */
	if(pData[DRV95HF_FRAME_OFFSET_COMMAND] == DRV95HF_COMMAND_ECHO) {
		/* Just echo byte is necessary: */
		drv95HF_SendReceiveByte(DRV95HF_COMMAND_ECHO);
	}
	else {
		/* CMD + LEN + DATA: */
		bufferlength = pData[DRV95HF_FRAME_OFFSET_LENGTH] + DRV95HF_FRAME_OFFSET_DATA;
		/* Transmit the buffer over SPI: */
		drv95HF_SendReceiveBuffer(pData, bufferlength, NULL);
	}

	/* Deselect the device over SPI: */
	DRV95HF_IO_CTRL_SET_SPI_SS();
}

/***************************************************************************//**
	@brief		This function polls the chip until a response is ready or
				the maximum number of attempts were performed.
	@return		Result code.
*******************************************************************************/

int8_t drv95HF_SPIPollingCommand(void) {

	int8_t error = RFTRANS_95HF_SUCCESS_CODE;
	uint8_t status = 0;
	uint16_t attempts = 0;

	/* Select the device over SPI: */
	DRV95HF_IO_CTRL_CLR_SPI_SS();

	/* Poll the transceiver until he's ready: */
	do {
		status = drv95HF_SendReceiveByte(DRV95HF_CONTROL_POLLING);
		status &= 0xF8;
		/* Checking the timeout: */
		if (attempts++ == DRV95HF_POLLING_ATTEMPTS) {
			error = RFTRANS_95HF_ERRORCODE_TIMEOUT;
			break;
		}
	} while(status != DRV95HF_FLAG_DATA_READY);

	/* Deselect the device over SPI: */
	DRV95HF_IO_CTRL_SET_SPI_SS();

	return error;
}

/***************************************************************************//**
	@brief		This function recovers a response from device.
	@param		pData Data pointer to receive.
	@return		None.
*******************************************************************************/

void drv95HF_ReceiveSPIResponse(uint8_t *pData) {

	uint16_t lengthToRead = 0;

	/* Select the device over SPI: */
	DRV95HF_IO_CTRL_CLR_SPI_SS();

	/* Request a response from transceiver: */
	drv95HF_SendReceiveByte(DRV95HF_CONTROL_RECEIVE);

	/* Recover the "Command" byte: */
	pData[DRV95HF_FRAME_OFFSET_COMMAND] = drv95HF_SendReceiveByte(DRV95HF_BYTE_DUMMY);

	/* Echo: */
	if(pData[DRV95HF_FRAME_OFFSET_COMMAND] == DRV95HF_COMMAND_ECHO) {
		pData[DRV95HF_FRAME_OFFSET_LENGTH]  = 0x00;
		/* In case we were in listen mode error code cancelled by user (0x85 0x00) must be retrieved: */
		pData[DRV95HF_FRAME_OFFSET_LENGTH+1] = drv95HF_SendReceiveByte(DRV95HF_BYTE_DUMMY);
		pData[DRV95HF_FRAME_OFFSET_LENGTH+2] = drv95HF_SendReceiveByte(DRV95HF_BYTE_DUMMY);
	}
	/* ST reserved: */
	else if(pData[DRV95HF_FRAME_OFFSET_COMMAND] == 0xFF) {
		pData[DRV95HF_FRAME_OFFSET_LENGTH]  = 0x00;
		pData[DRV95HF_FRAME_OFFSET_LENGTH + 1] = drv95HF_SendReceiveByte(DRV95HF_BYTE_DUMMY);
		pData[DRV95HF_FRAME_OFFSET_LENGTH + 2] = drv95HF_SendReceiveByte(DRV95HF_BYTE_DUMMY);
	}
	/* General data: */
	else {
		/* Recover the "Length" byte: */
		pData[DRV95HF_FRAME_OFFSET_LENGTH]  = drv95HF_SendReceiveByte(DRV95HF_BYTE_DUMMY);
		/* Checks the data length: */
		if(!(((pData[DRV95HF_FRAME_OFFSET_COMMAND] & 0xE0) == 0x80) && (pData[DRV95HF_FRAME_OFFSET_LENGTH] == 0x00))) {
			lengthToRead = (uint16_t)(pData[DRV95HF_FRAME_OFFSET_COMMAND] & 0x60);
			lengthToRead = (lengthToRead << 3) + pData[DRV95HF_FRAME_OFFSET_LENGTH];
			/* Recover data: */
			drv95HF_SendReceiveBuffer(NULL, lengthToRead, &pData[DRV95HF_FRAME_OFFSET_DATA]);
		}
	}

	/* Deselect the device over SPI: */
	DRV95HF_IO_CTRL_SET_SPI_SS();
	
}

/***************************************************************************//**
	@brief		This function send a command to 95HF device over SPI bus and
				receive its response.

	The structure of data is: 	CMD + LEN + DATA

	@param		pCommand	Command buffer.
	@param		pResponse	Receive buffer.
	@return		Result code.
*******************************************************************************/

int8_t drv95HF_SendReceive(uc8 *pCommand, uint8_t *pResponse) {

	int8_t error = RFTRANS_95HF_SUCCESS_CODE;
	
	/* Sending command: */
	drv95HF_SendSPICommand(pCommand);
	/* Poll the transceiver until he's ready: */
	if (drv95HF_SPIPollingCommand() == RFTRANS_95HF_SUCCESS_CODE) {
		/* Receiving bytes: */
		drv95HF_ReceiveSPIResponse(pResponse);
	}
	else {
		*pResponse = RFTRANS_95HF_ERRORCODE_TIMEOUT;
		error = RFTRANS_95HF_ERRORCODE_TIMEOUT;
	}

	return error;
}

/***************************************************************************//**
	@brief		Routine used after the POR.

	This routine is used after the Power-On Reset to send the ECHO command
	to verify the chip communication. The Echo command verifies the possibility
	of communication between a Host and the device.

	Host to Device -> 0x55;
	Device to Host -> 0x55;

	@return		Result code.
*******************************************************************************/

int8_t drv95HF_PowerOnReset(void) {

	int8_t error = RFTRANS_95HF_ERRORCODE_POR;

	uint32_t attempts = 0;
	uc8 command[]= {DRV95HF_COMMAND_ECHO};

	while ((error != RFTRANS_95HF_SUCCESS_CODE) && (attempts++ < DRV95HF_POR_ATTEMPTS)) {

		/* SPI reset: */
		drv95HF_WakeUp();

		/* Send an ECHO command: */
		drv95HF_SendReceive(command, u95HFBuffer);

		/* Echo response verification: */
		if (u95HFBuffer[0] == DRV95HF_COMMAND_ECHO) {
			error = RFTRANS_95HF_SUCCESS_CODE;
		}

	}

	return error;

}

/***************************************************************************//**
	@brief		Sends IDN command.

	The IDN command (0x01) gives brief information about the device and its
	revision. The value is stored in the "IcVers" register.

	@return		Result code.
*******************************************************************************/

int8_t drv95HF_Information(void) {

	int8_t error = RFTRANS_95HF_SUCCESS_CODE;
	uc8 command[] = {DRV95HF_COMMAND_IDN, 0x00};

	/* Send the command and retrieve its response: */
	error = drv95HF_SendReceive(command, u95HFBuffer);

	/* Getting version: */
	if (error == RFTRANS_95HF_SUCCESS_CODE) {
		IcVers = (IC_VERSION)(u95HFBuffer[ROM_CODE_REVISION_OFFSET]);
		if (IcVers < QJA && IcVers > QJE) {
			error = RFTRANS_95HF_ERRORCODE_DEFAULT;
		}
	}

	return error;

}

//----------------------------------------------------------------------------//
