/***************************************************************************//**
	@file		nfc.c
	@brief		CR95HF or ST25R95 driver.
********************************************************************************

	This driver is used to manage the TAGS.

	@see		https://www.st.com/en/embedded-software/stsw-m24lr007.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		September-2020.
*******************************************************************************/

#include "nfc.h"

#include "crypto1.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/**
 * Start time [ms].
 */
#define NFC_HANDLE_START_TIME	5

/**
 * Hunt time [ms].
 */
#define NFC_HANDLE_FIND_TIME	0

/**
 * uint8_t vector to uint32_t value.
 */
#define _uint8_to_uint32(x, i)	(uint32_t)((x[i+0] << 24) | (x[i+1] << 16) | (x[i+2] << 8) | (x[i+3]))

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/* Internal registers: */
volatile static uint32_t NfcHandleCounter = 0;

/* Variables for the different modes: */
DeviceMode_t devicemode = UNDEFINED_MODE;
TagType_t nfc_tagtype = UNDEFINED_TAG_TYPE;

extern uint8_t u95HFBuffer[RFTRANS_95HF_MAX_BUFFER_SIZE+3];
extern ISO14443A_CARD ISO14443A_Card;

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

static void Nfc_Event(nfc_handle_t *h, const nfc_event_t event, const uint8_t *data);
static void Nfc_Handle_Start(nfc_handle_t *h);
static void Nfc_Handle_Find(nfc_handle_t *h);

/***************************************************************************//**
	@brief		Useful routine to call the event callback.
	@param		h Driver handle structure.
	@param		event	Event (@ref nfc_event_t).
	@param		data	Data according with the event.
	@return		None.
*******************************************************************************/

static void Nfc_Event(nfc_handle_t *h, const nfc_event_t event, const uint8_t *data)  {

	if (h->callbackEvent != NULL) {
		h->callbackEvent(event, data);
	}

}

/***************************************************************************//**
	@brief		State used to start the process to find.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Start(nfc_handle_t *h) {

	/* Next tag: */
	if (h->tag != NFC_TRACK_LAST) {
		h->tag <<= 1;
	}
	else {
		h->tag = NFC_TRACK_FIRST;
	}

	/* Check if is a desired tag: */
	if (h->tag & h->tagsToFind) {

		/* Turn off (require before tag initialization)*/
		PCD_FieldOff();

		/* Time to start: */
		NfcHandleCounter = NFC_HANDLE_START_TIME;
		h->state = NFC_HANDLE_STATE_FIND;

	}

}

/***************************************************************************//**
	@brief		Crypto1 data to ST25R95 data.
	@param		src		Crypto1 data (with parity).
	@param		dest	ST25R95 buffer.
	@param		Size	Source size.
	@return		None.
*******************************************************************************/

void Nfc_Crypto1_Parity_To_Vector(const parity_data_t *src, uint8_t *dest, const uint32_t size) {

	for (uint32_t i = 0; i < size; i++) {
		*dest++ = (uint8_t)*src;
		if (*src++ & 0x100) {
			/* Parity 1 of ST25R95: */
			*dest++ = 0x80;
		}
		else {
			/* Parity 0 of ST25R95: */
			*dest++ = 0x00;
		}
	}

}

/***************************************************************************//**
	@brief		ST25R95 data to Crypto1 data.
	@param		src 	ST25R95 data (with parity).
	@param		dest	Crypto1 buffer.
	@param		size	Destination size.
	@return		None.
*******************************************************************************/

void Nfc_Crypto1_Vector_To_Parity(const uint8_t *src, parity_data_t *dest, const uint32_t size) {

	for (uint32_t i = 0; i < size; i++) {
		*dest = *src++;
		if (*src++ == 0x80) {
			*dest++ |= 0x100;
		}
		else {
			dest++;
		}
	}

}

/***************************************************************************//**
	@brief		Calculate CRC-A and return it.
	@param		data	Data pointer.
	@len		len		Data size.
	@return		CRC-A.
*******************************************************************************/

uint16_t Nfc_Mifare_CRC(const parity_data_t *data, const uint32_t len) {

	uint32_t i, j;

	uint16_t crc = 0x6363;
	for (i = 0; i < len; i++) {
		uint8_t byte = data[i] & 0xff;
		for (j = 0; j < 8; j++) {
			char bit = (crc ^ byte) & 1;
			byte >>= 1;
			crc >>= 1;
			if (bit) crc ^= 0x8408;
		}
	}

	return crc;

}

/***************************************************************************//**
	@brief		Calculate the parity.
	@param		data	Data pointer.
	@len		len		Data size.
	@return		None.
*******************************************************************************/

void Nfc_Mifare_Parity(parity_data_t *data, const uint32_t len) {

	uint32_t i;

	for (i = 0; i < len; i++) {
		data[i] = (data[i] & 0x00FF) | (ODD_PARITY(data[i]) << 8);
	}

}

/***************************************************************************//**
	@brief		Parity and CRC check.
	@param		data	Data pointer.
	@len		len		Data size.
	@return		TRUE to successful and FALSE to failure.
*******************************************************************************/

uint8_t Nfc_Mifare_Check(parity_data_t *data, const uint32_t len) {

	uint8_t ret = 1;
	uint16_t crcReceived, crcCalculed;
	uint32_t i;

	if (len > 2) {
		for (i = 0; i < len; i++) {
			if (ODD_PARITY(data[i] & 0x00FF) != (data[i] >> 8)) {
				ret = 0;
				break;
			}
		}
		if (ret) {
			crcReceived = ((data[len-1] & 0x00FF) << 8) | (data[len-2] & 0x00FF);
			crcCalculed = Nfc_Mifare_CRC(data, len - 2);
			if (crcReceived != crcCalculed) {
				ret = 0;
			}
		}
	}
	else {
		ret = 0;
	}

	return ret;

}

/***************************************************************************//**
	@brief		MIFARE authentication procedure.
	@param		cryptoState	Crypto1 state.
	@param		type		0x60 to KEYA and 0x61 to KEYB.
	@param		key			48 bits key.
	@param		block		Block address.
	@return		TRUE to successful and FALSE to failure.
*******************************************************************************/

uint8_t Nfc_Mifare_Auth(crypto1_state *cryptoState, const uint8_t type, const uint64_t key, const uint8_t block) {

	uint8_t data[17];
	parity_data_t reader_response[8];
	parity_data_t card_response[4];

	/* Start: */
	uc8 param[] = {type, block, PCD_ISO14443A_APPENDCRC | PCD_ISO14443A_A8BITSINFIRSTBYTE};
	PCD_SendRecv(sizeof(param), param, u95HFBuffer);
	if ((u95HFBuffer[0] != 0x80) && (u95HFBuffer[1] != 0x07)) {
		return 0;
	}

	/* Crypto1 initialization: */
	crypto1_new(cryptoState, 0, CRYPTO1_IMPLEMENTATION_CLEAN);

	/* First step (KEY + uid + nonce tag): */
	crypto1_init(cryptoState, key);
	crypto1_mutual_1(cryptoState, _uint8_to_uint32(ISO14443A_Card.UID, 0), _uint8_to_uint32(u95HFBuffer, 2));

	/* Second step (nonce reader): */
	UINT32_TO_ARRAY_WITH_PARITY(0x44332211, reader_response);
	if(!crypto1_mutual_2(cryptoState, reader_response)) {
		return 0;
	}
	Nfc_Crypto1_Parity_To_Vector(reader_response, data, 8);
	data[16] = 0x10 | PCD_ISO14443A_A8BITSINFIRSTBYTE;
	PCD_SendRecv(17, data, u95HFBuffer);
	if ((u95HFBuffer[0] != 0x80) && (u95HFBuffer[1] != 0x0B)) {
		return 0;
	}

	/* Card response validate validate: */
	Nfc_Crypto1_Vector_To_Parity(&u95HFBuffer[2], card_response, 4);
	if(!crypto1_mutual_3(cryptoState, card_response)) {
		return 0;
	}

	return 1;

}

/***************************************************************************//**
	@brief		MIFARE Classic read procedure.
	@param		cryptoState	Crypto1 state.
	@param		block		Block address.
	@param		data		16 bytes buffer to store the block data.
	@return		TRUE to successful and FALSE to failure.
*******************************************************************************/

uint8_t Nfc_Mifare_Read(crypto1_state *cryptoState, const uint8_t block, uint8_t *data) {

	uint16_t crc;
	parity_data_t cmd[] = {0x30, 0x00, 0x00, 0x00};
	uint8_t vector[9];
	parity_data_t resp[18];

	/* Getting the command with CRC: */
	cmd[1] = block;
	crc = Nfc_Mifare_CRC(cmd, 2);
	cmd[2] = crc & 0x00FF;	/* LSB. */
	cmd[3] = crc >> 8;		/* MSB. */

	/* Parity: */
	Nfc_Mifare_Parity(cmd, sizeof(cmd)/sizeof(parity_data_t));

	/* Read command: */
	crypto1_transcrypt(cryptoState, cmd, sizeof(cmd)/sizeof(parity_data_t));
	Nfc_Crypto1_Parity_To_Vector(cmd, vector, 4);
	vector[8] = 0x10 | PCD_ISO14443A_A8BITSINFIRSTBYTE;
	PCD_SendRecv(9, vector, u95HFBuffer);
	if ((u95HFBuffer[0] != 0x80) && (u95HFBuffer[1] != 0x27)) {
		return 0;
	}

	/* Decrypt: */
	Nfc_Crypto1_Vector_To_Parity(&u95HFBuffer[2], resp, sizeof(resp)/sizeof(parity_data_t));
	crypto1_transcrypt(cryptoState, resp, sizeof(resp)/sizeof(parity_data_t));

	/* Parity and CRC check */
	if (!Nfc_Mifare_Check(resp, sizeof(resp)/sizeof(parity_data_t))) {
		return 0;
	}

	/* Convert: */
	for (uint32_t i = 0; i < 16; i++) {
		data[i] = (uint8_t)resp[i];
	}

	return 1;

}

/***************************************************************************//**
	@brief		MIFARE Classic read procedure.
	@param		cryptoState	Crypto1 state.
	@param		block		Block address.
	@param		data		16 bytes buffer to store the block data.
	@return		TRUE to successful and FALSE to failure.
*******************************************************************************/

uint8_t Nfc_Mifare_Write(crypto1_state *cryptoState, const uint8_t block, const uint8_t *data) {

	uint16_t crc;
	parity_data_t cmd[] = {0xA0, 0x00, 0x00, 0x00};
	uint8_t vector[9];
	parity_data_t wrParity[18];
	uint8_t wrBuffer[37];

	parity_data_t ackTemp;
	crypto1_state cryptoStateTemp;

	/* Getting the command with CRC: */
	cmd[1] = block;
	crc = Nfc_Mifare_CRC(cmd, 2);
	cmd[2] = crc & 0x00FF;	/* LSB. */
	cmd[3] = crc >> 8;		/* MSB. */

	/* Parity: */
	Nfc_Mifare_Parity(cmd, sizeof(cmd)/sizeof(parity_data_t));

	/* Write command: */
	crypto1_transcrypt(cryptoState, cmd, sizeof(cmd)/sizeof(parity_data_t));
	Nfc_Crypto1_Parity_To_Vector(cmd, vector, 4);
	vector[8] = 0x10 | PCD_ISO14443A_A8BITSINFIRSTBYTE;
	PCD_SendRecv(9, vector, u95HFBuffer);
	if ((u95HFBuffer[0] != 0x90) && (u95HFBuffer[1] != 0x04)) {
		return 0;
	}

	/**
	 * @note 	This tranceiver receives the encrypted 4-bits ACK and
	 * 		 	interprets like NACK and do not send the data, so,
	 * 		 	the byte is emulated here to restore the Crypto1 state.
	 */
	cryptoStateTemp = *cryptoState;
	ackTemp = 0x0A;
	crypto1_transcrypt_bits(&cryptoStateTemp, &ackTemp, 0, 4);
	crypto1_transcrypt_bits(cryptoState, &ackTemp, 0, 4);

	/* Data: */
	for (uint32_t i = 0; i < 16; i++) {
		wrParity[i] = data[i];
	}
	crc = Nfc_Mifare_CRC(wrParity, 16);
	wrParity[16] = crc & 0x00FF;	/* LSB. */
	wrParity[17] = crc >> 8;		/* MSB. */
	Nfc_Mifare_Parity(wrParity, sizeof(wrParity)/sizeof(parity_data_t));
	crypto1_transcrypt(cryptoState, wrParity, sizeof(wrParity)/sizeof(parity_data_t));
	Nfc_Crypto1_Parity_To_Vector(wrParity, wrBuffer, 18);
	wrBuffer[36] = 0x10 | PCD_ISO14443A_A8BITSINFIRSTBYTE;
	PCD_SendRecv(37, wrBuffer, u95HFBuffer);

	return 1;

}

/***************************************************************************//**
	@brief		Find the TAG.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Find(nfc_handle_t *h) {

	uint8_t TagUID[16];
	uint8_t data[] = {NFC_TRACK_NOTHING};

	devicemode = UNDEFINED_MODE;
	nfc_tagtype = UNDEFINED_TAG_TYPE;

	crypto1_state cryptoState;
	uint8_t mifareData[16];
	static uint8_t write = 0;
	const uint8_t block = 0x04;
	const uint8_t writeData[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};

	/* NFC type 1: */
	if (h->tag & NFC_TRACK_NFCTYPE1) {
		ISO14443A_Init();
		if(ISO14443A_IsPresent() == RESULTOK) {
			if(TOPAZ_ID(TagUID) == RESULTOK) {
				data[0] = NFC_TRACK_NFCTYPE1;
				Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
			}
		}
	}
	/* NFC type 2 and 4A: */
	else if ((h->tag & NFC_TRACK_NFCTYPE2) || (h->tag & NFC_TRACK_NFCTYPE4A)) {
		ISO14443A_Init( );
		if(ISO14443A_IsPresent() == RESULTOK) {
			if(ISO14443A_Anticollision() == RESULTOK) {
				if (((ISO14443A_Card.SAK&0x60) == 0x00) && (h->tag & NFC_TRACK_NFCTYPE2)) {
					if (Nfc_Mifare_Auth(&cryptoState, 0x61, 0x0000FFFFFFFFFFFF, block)) {
						if (Nfc_Mifare_Read(&cryptoState, block, mifareData)) {
							data[0] = NFC_TRACK_NFCTYPE2;
							Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
						}
						if (write) {
							write = 0;
							if (Nfc_Mifare_Write(&cryptoState, block, writeData)) {

							}
						}
					}
				}
				else if (((ISO14443A_Card.SAK&0x20) != 0x00) && (h->tag & NFC_TRACK_NFCTYPE4A)) {
					data[0] = NFC_TRACK_NFCTYPE4A;
					Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
				}
			}
		}
	}
	/* NFC type 3: */
	else if (h->tag & NFC_TRACK_NFCTYPE3) {
		FELICA_Initialization();
		if (FELICA_IsPresent() == RESULTOK) {
			data[0] = NFC_TRACK_NFCTYPE3;
			Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
		}
	}
	/*  NFC type 4B: */
	else if (h->tag & NFC_TRACK_NFCTYPE4B) {
		if(ISO14443B_IsPresent() == RESULTOK ) {
			if(ISO14443B_Anticollision() == RESULTOK) {
				data[0] = NFC_TRACK_NFCTYPE4B;
				Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
			}
		}
	}
	/*  ISO15693: */
	else if (h->tag & NFC_TRACK_NFCTYPE5) {
		if(ISO15693_GetUID (TagUID) == RESULTOK) {
			data[0] = NFC_TRACK_NFCTYPE5;
			Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
		}
	}

	/* Turn off the field if no tag has been detected: */
	if (data[0] == NFC_TRACK_NOTHING) {
		PCD_FieldOff();
		Nfc_Event(h, NFC_EVENT_TAG_FOUND, data);
	}

	/* Return: */
	NfcHandleCounter = NFC_HANDLE_FIND_TIME;
	h->state = NFC_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		This function initialize the NFC chip.
	@param		h				Driver handle structure.
	@param		callbackEvent	Event callback.
	@param		tagsToFind		Tags to find.
	@return		Driver error code (@ref nfc_error_t).
*******************************************************************************/

nfc_error_t Nfc_Init(nfc_handle_t *h, const nfc_callback_event_t callbackEvent, const uint8_t tagsToFind) {

	nfc_error_t  error = NFC_ERROR_OK;
	
	NfcHandleCounter = 0;
	h->state = NFC_HANDLE_STATE_START;
	h->callbackEvent = callbackEvent;
	h->tagsToFind = tagsToFind;
	h->tag = NFC_TRACK_LAST;

	/* Initialize the RF transceiver: */
	if (drv95HF_PowerOnReset() != RFTRANS_95HF_SUCCESS_CODE) {
		error = NFC_ERROR_INIT;
	}

	/* Retrieve the IC version of the chip: */
	if (error == NFC_ERROR_OK) {
		if (drv95HF_Information() != RFTRANS_95HF_SUCCESS_CODE) {
			error = NFC_ERROR_INIT;
		}
	}

	return error;

}

/***************************************************************************//**
	@brief		Driver time base.

	Must be run in a 1ms routine.

	@return		None.
*******************************************************************************/

void Nfc_Tick(void) {

	if (NfcHandleCounter) NfcHandleCounter--;

}

/***************************************************************************//**
	@brief		Driver manager.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_Handle(nfc_handle_t *h) {

	/* State machine routines: */
	static void (* const fnc[])(nfc_handle_t *h) = {
		Nfc_Handle_Start,	/* NFC_HANDLE_STATE_START. */
		Nfc_Handle_Find,	/* NFC_HANDLE_STATE_FIND. */
	};

	/* Manager state machine: */
	if (h->state < (sizeof (fnc)/sizeof(fnc[h->state])) && (!NfcHandleCounter)) {
		fnc[h->state](h);
	}

}

//----------------------------------------------------------------------------//
