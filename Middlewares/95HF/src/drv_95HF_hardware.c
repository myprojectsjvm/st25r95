/***************************************************************************//**
	@file		drv_95HF_hardware.c
	@brief		Routines related with the hardware.
********************************************************************************
	This driver is used to facilitate the library portability.

	@note		STM32F.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		September-2020.
*******************************************************************************/

#include "drv_95HF_hardware.h"
#include "drv_95HF.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/**
 * Useful macro to get a minimum value.
 */
#define _drv95HF_min(x,y)			((x < y)? x : y)

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/***************************************************************************//**
	@brief		Useful routine to manage the errors.

	Put your error handle here.

	@param		error Error code (@ref drv95HF_error_t).
	@return		None.
*******************************************************************************/

void drv95HF_Error(drv95HF_error_t error) {

}

/***************************************************************************//**
	@brief		Useful routine to send and receive a single byte at the same
				time.
	@param		data Data to be sent.
	@return		Received data.
*******************************************************************************/

uint8_t drv95HF_SendReceiveByte(uint8_t data) {

	uint8_t dataTx[1];
	uint8_t dataRx[1];

	dataTx[0] = data;
	dataRx[0] = 0xFF;

	if (HAL_SPI_TransmitReceive(DRV95HF_SPI_INSTANCE, dataTx, dataRx, 1, DRV95HF_SPI_TIMEOUT) != DRV95HF_SPI_OK) {
		/* An error is not common here, so this is just for a debug purpose: */
		drv95HF_Error(DRV95HF_ERROR_SPI);
	}

	return dataRx[0];

}

/***************************************************************************//**
	@brief		Useful routine used to send and receive through the SPI.
	@param		pCommand	Command pointer (NULL is possible).
	@param		length		Pointer size.
	@param		pResponse	Response pointer (NULL is possible).
	@return		None.
*******************************************************************************/

void drv95HF_SendReceiveBuffer(uc8 *pCommand, uint16_t length, uint8_t *pResponse) {

	uint8_t command = 0;

	/* The buffer size is limited to MAX_BUFFER_SIZE: */
	length = _drv95HF_min(MAX_BUFFER_SIZE,length);
	for (uint32_t i = 0; i < length; i++) {
		/* When dummy is sent the pointer is NULL: */
		if (pCommand != NULL) {
			command = pCommand[i];
		}
		/* When dummy is received the pointer is NULL: */
		if (pResponse != NULL) {
			pResponse[i] = drv95HF_SendReceiveByte(command);
		}
		else {
			 drv95HF_SendReceiveByte(command);
		}
	}
}

//----------------------------------------------------------------------------//
