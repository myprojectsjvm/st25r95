/***************************************************************************//**
	@file		nfc.h
	@brief		CR95HF or ST25R95 driver.
********************************************************************************

	This driver is used to manage the TAGS.

	@see		https://www.st.com/en/embedded-software/stsw-m24lr007.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		September-2020.
*******************************************************************************/

#ifndef NFC_H_
#define NFC_H_

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "lib_iso15693pcd.h"
#include "lib_iso14443Apcd.h"
#include "lib_iso14443Bpcd.h"
#include "lib_iso18092pcd.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Flags for PCD tracking.
 */
typedef enum {

	NFC_TRACK_NOTHING 	= 0x00, 				/* 0000 0000 */
	NFC_TRACK_NFCTYPE1 	= 0x01, 				/* 0000 0001 */
	NFC_TRACK_NFCTYPE2 	= 0x02, 				/* 0000 0010 */
	NFC_TRACK_NFCTYPE3 	= 0x04, 				/* 0000 0100 */
	NFC_TRACK_NFCTYPE4A = 0x08, 				/* 0000 1000 */
	NFC_TRACK_NFCTYPE4B = 0x10, 				/* 0001 0000 */
	NFC_TRACK_NFCTYPE5 	= 0x20, 				/* 0010 0000 */
	NFC_TRACK_ALL 		= 0x3F, 				/* 0011 1111 */
	NFC_TRACK_FIRST 	= NFC_TRACK_NFCTYPE1, 	/* 0000 0001 (internal use) */
	NFC_TRACK_LAST 		= NFC_TRACK_NFCTYPE5	/* 0010 0000 (internal use) */

} nfc_track_t;

/**
 * Handle states.
 */
typedef enum {

	NFC_HANDLE_STATE_START = 0,			/**< Start the process. */
	NFC_HANDLE_STATE_FIND,				/**< Tag hunting. */
	NFC_HANDLE_STATE_MAX				/**< Limitation. */

} nfc_handle_state_t;

/**
 * Driver error codes.
 */
typedef enum {

	NFC_ERROR_OK = 0x00,				/**< Successful indication. */
	NFC_ERROR_FAIL,						/**< Failure indication. */
	NFC_ERROR_INIT						/**< Initialization error. */

} nfc_error_t;

/**
 * Events.
 */
typedef enum {

	NFC_EVENT_TAG_FOUND = 0x00			/**< Indicates that the TAG was found. */

} nfc_event_t;

/**
 * Event callback.
 *
 * @param event Event (@ref nfc_event_t).
 * @param data	Data according with the event.
 */
typedef void (*nfc_callback_event_t)(const nfc_event_t event, const uint8_t *data);

/**
 * Driver handle.
 */
typedef struct {

	nfc_handle_state_t state;			/**< Handle state. */
	nfc_callback_event_t callbackEvent; /**< Event callback. */
	uint8_t tagsToFind;					/**< Tags to find. */
	uint8_t tag;						/**< Tag to find. */

} nfc_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

nfc_error_t Nfc_Init(nfc_handle_t *h, const nfc_callback_event_t callbackEvent, const uint8_t tagsToFind);
void Nfc_Tick(void);
void Nfc_Handle(nfc_handle_t *h);

//----------------------------------------------------------------------------//

#endif /* NFC_H_ */

//----------------------------------------------------------------------------//
