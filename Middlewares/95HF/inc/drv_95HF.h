/***************************************************************************//**
	@file		drv_95HF.c
	@brief		CR95HF or ST25R95 driver.
********************************************************************************

	This driver is used to control the CR95HF or the ST25R95 NFC transceiver
	over SPI bus.

	@see		https://www.st.com/en/embedded-software/stsw-m24lr007.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		September-2020.
*******************************************************************************/

#ifndef DRV_95HF_H_
#define DRV_95HF_H_

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"
#include "drv_95HF_hardware.h"
#include "miscellaneous.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/* Control: */
#define DRV95HF_CONTROL_SEND				0x00	/**< Send command to the device. */
#define DRV95HF_CONTROL_RESET				0x01	/**< Reset the device. */
#define DRV95HF_CONTROL_RECEIVE				0x02	/**< Read data from the device. */
#define DRV95HF_CONTROL_POLLING				0x03	/**< Poll the device. */

/* Commands: */
#define DRV95HF_COMMAND_IDN					0x01	/**< Requests short information about the device and its revision. */
#define DRV95HF_COMMAND_IDLE				0x07	/**< Switches the device into a low consumption. */
#define DRV95HF_COMMAND_ECHO				0x55	/**< Performs a serial interface Echo command. */

/* Flags: */
#define DRV95HF_FLAG_DATA_READY				0x08	/**< Data can be read from the device. */
#define DRV95HF_FLAG_DATA_READY_MASK		0x08	/**< Data can be read from the device. */

/* Frame: */
#define DRV95HF_FRAME_OFFSET_COMMAND		0x00	/**< FRAME = CMD + LEN + DATA. */
#define DRV95HF_FRAME_OFFSET_LENGTH			0x01	/**< FRAME = CMD + LEN + DATA. */
#define DRV95HF_FRAME_OFFSET_DATA			0x02	/**< FRAME = CMD + LEN + DATA. */

/* General: */
#define DRV95HF_POLLING_ATTEMPTS			1000	/**< Maximum number of attempts in the polling. */
#define DRV95HF_POR_ATTEMPTS				5		/**< Maximum number of attempts in the POR. */
#define DRV95HF_BYTE_DUMMY					0xFF	/**< General dummy byte. */

/* Calibration: */
#define WU_SOURCE_OFFSET					0x02
#define WU_PERIOD_OFFSET					0x09
#define DACDATAL_OFFSET						0x0C
#define DACDATAH_OFFSET						0x0D
#define NBTRIALS_OFFSET						0x0F

/* Returns: */
#define RFTRANS_95HF_SUCCESS_CODE			RESULTOK
#define RFTRANS_95HF_NOREPLY_CODE			0x01
#define	RFTRANS_95HF_ERRORCODE_DEFAULT		0xFE
#define	RFTRANS_95HF_ERRORCODE_TIMEOUT		0xFD
#define RFTRANS_95HF_ERRORCODE_POR			0x44

/* Others: */
#define IDLE_CMD_LENTH						0x0E
#define MAX_BUFFER_SIZE  					528
#define RFTRANS_95HF_MAX_BUFFER_SIZE		0x20E
#define ROM_CODE_REVISION_OFFSET			13

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver version.
 */
typedef enum {
	QJA = 0x30,
	QJB,
	QJC,
	QJD,
	QJE
} IC_VERSION;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

void drv95HF_SendIRQINPulse(void);
void drv95HF_ResetSPI(void);
void drv95HF_WakeUp(void);
void drv95HF_SendSPICommand(uc8 *pData);
int8_t drv95HF_SPIPollingCommand(void);
void drv95HF_ReceiveSPIResponse(uint8_t *pData);
int8_t drv95HF_SendReceive(uc8 *pCommand, uint8_t *pResponse);
int8_t drv95HF_PowerOnReset(void);
int8_t drv95HF_Information(void);

//----------------------------------------------------------------------------//

#endif /* DRV_95HF_H_ */

//----------------------------------------------------------------------------//
