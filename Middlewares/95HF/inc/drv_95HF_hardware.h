/***************************************************************************//**
	@file		drv_95HF_hardware.h
	@brief		Routines related with the hardware.
********************************************************************************
	This driver is used to facilitate the library portability.

	@note		STM32F.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		September-2020.
*******************************************************************************/

#ifndef DRV_95HF_HARDWARE_H_
#define DRV_95HF_HARDWARE_H_

#include "mcu_includes.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/**
 * IO clear control to SPI_SS pin.
 */
#define DRV95HF_IO_CTRL_CLR_SPI_SS() 		HAL_Delay(1); \
											IO_CTRL_CLR_CR95HF_SPI_SS; \
											HAL_Delay(1)

/**
 * IO set control to SPI_SS pin.
 */
#define DRV95HF_IO_CTRL_SET_SPI_SS()  		HAL_Delay(1); \
											IO_CTRL_SET_CR95HF_SPI_SS; \
											HAL_Delay(1)

/**
 * IO clear control to IRQ_IN pin.
 */
#define DRV95HF_IO_CTRL_CLR_IRQ_IN() 		IO_CTRL_CLR_CR95HF_IRQ_IN

/**
 * IO set control to IRQ_IN pin.
 */
#define DRV95HF_IO_CTRL_SET_IRQ_IN()  		IO_CTRL_SET_CR95HF_IRQ_IN

/**
 * SPI handle.
 */
#define DRV95HF_SPI_INSTANCE				&hspi1

/**
 * Timeout in the SPI (x1ms).
 */
#define DRV95HF_SPI_TIMEOUT					100

/**
 * SPI successful return.
 */
#define DRV95HF_SPI_OK						HAL_OK

/**
 * Delay used in the initialization.
 */
#define DRV95HF_DELAY_MS(x)					HAL_Delay(x)

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver error codes.
 */
typedef enum {

	DRV95HF_ERROR_OK = 0x00,				/**< Successful indication. */
	DRV95HF_ERROR_SPI,						/**< SPI error indication. */

} drv95HF_error_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

void drv95HF_Error(drv95HF_error_t error);
uint8_t drv95HF_SendReceiveByte(uint8_t data);
void drv95HF_SendReceiveBuffer(uc8 *pCommand, uint16_t length, uint8_t *pResponse);

//----------------------------------------------------------------------------//

#endif /* DRV_95HF_HARDWARE_H_ */

//----------------------------------------------------------------------------//
