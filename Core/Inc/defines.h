/***************************************************************************//**
	@file		defines.h
	@brief		General defines.
********************************************************************************
	General defines.

	@note		S32 Design Studio for ARM.

	@note		S32K microcontrollers.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#ifndef DEFINES_H_
#define DEFINES_H_

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

#define delay_ms(x)					HAL_Delay(x)
#define delayHighPriority_ms(x)		HAL_Delay(x)

/** @todo CR95HF us delay. */
#define delay_us(x)					HAL_Delay(x)

/*******************************************************************************
	DEFINES:
*******************************************************************************/

#define MCU_DELAY_MS(x)				HAL_Delay(x)

#define MCU_IO_CTRL_SET_LED			HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET)
#define MCU_IO_CTRL_CLR_LED			HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET)
#define MCU_IO_CTRL_TGL_LED			HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin)

#define IO_CTRL_SET_CR95HF_SPI_SS	HAL_GPIO_WritePin(NFC_SPI_SS_GPIO_Port, NFC_SPI_SS_Pin, GPIO_PIN_SET)
#define IO_CTRL_SET_CR95HF_IRQ_IN	HAL_GPIO_WritePin(NFC_IRQ_IN_GPIO_Port, NFC_IRQ_IN_Pin, GPIO_PIN_SET)

#define IO_CTRL_CLR_CR95HF_SPI_SS	HAL_GPIO_WritePin(NFC_SPI_SS_GPIO_Port, NFC_SPI_SS_Pin, GPIO_PIN_RESET)
#define IO_CTRL_CLR_CR95HF_IRQ_IN	HAL_GPIO_WritePin(NFC_IRQ_IN_GPIO_Port, NFC_IRQ_IN_Pin, GPIO_PIN_RESET)

/* ISO14443A: */
#define PCD_TYPEA_ARConfigA	0x01
#define PCD_TYPEA_ARConfigB	0xD0
#define PCD_TYPEA_TIMERW    0x58

/* ISO14443B: */
#define PCD_TYPEB_ARConfigA	0x01
#define PCD_TYPEB_ARConfigB	0x51

/* Felica: */
#define PCD_TYPEF_ARConfigA	0x01
#define PCD_TYPEF_ARConfigB	0x51

/* ISO15693: */
#define PCD_TYPEV_ARConfigA	0x01
#define PCD_TYPEV_ARConfigB	0xD0

//----------------------------------------------------------------------------//

#endif /* DEFINES_H_ */

//----------------------------------------------------------------------------//
